---
title: "Caracterizaão da área dos assentamentos de Palmas, através de dados georeferenciados e sofwares disponíveis na internet"
author: "Adenor Vicente Wendling"
date: "25/02/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

O projeto em desenvolvimento tem dois objetivos:


# 1.  Elaborar um roteiro, com tutoriais em R, para elaborar mapas temáticos dos assentamentos São Lourenço, Paraíso do Sul e Margem do Irati, de Palmas, PR, com os seguintes capítulos:


> Elaboração do mapa em Qgis, com importação dos arquivos shapefiles e rastes pertinentes.


> Mesclar dados dos arquivos shapefile e de arquivos texto (.csv)


> Recortar e aplicar propriedades para visualização;


> Elaboraração de mapas das características do solo, com base nas alálises realizadas em laboratório;


> Impressão dos mapas


# 2. Elaborar um documento com os dados do Assentamento Sâo Lourenço, abrangendo dados topográficos, ambientais, climáticos e das características do solo. 

As características químicas do solo referem-se a 66 amostras, coletadas em 28 assentados, analisadas no laboratório de solos do IFPR - campus Palmas. As demais características são oriundas dos mapas dos bancos de dados acessados através do QGIS.

# Elaborados
## Tutoriais
1. [Tutorial 1 - Extrair e manipular dados do KOBO](tutoriais/Tutorial_1_OrganizarDadosColetaKobo.Rmd)
Durante o trabalho de identificação dos assentamentos foram coletados amostras de solos, e, através do aplicativo KOBOTOOLS, aplicado um questionário sobre a área. Tanto o ponto de coleta, quanto as informaõções do questionário foram extraídas e interpretadas através desse tutorial.

1. [Tutorial 2 - Como importar e inserir arquivos de diversas fontes](tutoriais/Tutorial_2_Baixar_Inserir_Camadas.Rmd)
Para elaborar o relatório com as características da área dos assentamentos, foram importandos inúmeros arquivos vetoriais e matriciais. Neste tutorial são descritos todos os passos para importar, recortar e manipular esses dados para compor os mapas temáticos da área.

1. [Tutorial 4 - Cálculos de porcentagens de arquivos raster](tutoriais/Tutorial_4_Calculos de Porcentagem.Rmd)


1. [Tutorial 5- Orientações para interpolação de dados de fertilidades de solos](tutoriais/Tutorial_5_interpolaçao.Rmd)
Um dos objeitvos primeiros do projeto é de elaborar mapas cindicando as característcas dos solos agrícolas dos assentamentos, com os dados das análises dos solos coletados. Através da interpolaão foram gerados mapas para cada componente importante do solo. O passo a passo do processo de interpolaão, bem como o significado de cada mapa são apresentados neste tutorial.
1. [Tutorial 6 - Interpretação de análise de solo para a cultura da maça, baseado no manual de calagem e adubação de SC](tutoriais/Tutorial_6_Macieira.Rmd)

1. [Tutorial 7 - Como orirentações para a produção de TCC via R](tutorial/Tutorial_7_produzir arquivo formato TCC.Rmd)
1. [Tutorial 8 - Elaboração dos mapas através do QGIS]()

## Relatórios
1. [Relatório 2 - Estudos dos assentamentos de Palmas, PR ](Relatos/primeiro_relatório.html)

## Mapas em Qgis
1. [Mapa com várias camadas de interesse](data/Coleta_Assentamento_SL_1.csv)

## Em elaboração
1. Falta fazer análise dos dados de análise do solo

## Script do R
1. [O relatório para defesa do projeto](Relatos/RelatorioProjetoAssentamento.Rmd)




# 3. Observações
A bibliografia deve ser inserida do arquivo Fruti.bid.
O estilo de referencias está no arquivo my-stules2.odt