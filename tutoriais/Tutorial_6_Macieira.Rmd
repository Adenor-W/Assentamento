---
title: "Recomendações adubação macieira"
author: "Adenor Vicente Wendling"
date: "06/08/2021"
output: html_document
bibliography: ../Fruti.bib
csl: ../abnt.csl
---

Este documento tem como finalizade buscar uma recomendação adequada para a implantação e desenvolvimento da cultura da macieira na propriedade de IFPR, em Palmas.
As características do solo e resultados da são:

Antes da recomendação, entretanto, será feita uma busca em literatura especializada, especialmente com uso de adubações orgânicas.

# Revisão
Artigo de [@Miele2017] analisou o uso de super fosfato triplo, e concluiu que, se houve uma adubação de correção adequada na implantação, não há aumento de produtividade com a aplicação de fosfato durante os primeiros 10 anos de produção.
[@DeSouza2013] estudaram uso de N e K²O com diversas dosagens na qualidade dos frutos mas as evidencias cosntatadas não me convenceram do seu benefício.

No Manual de adubação e calagem para o estado do Paraná [@PR2017] não há recomendação específica para a cultura da macieira. Por isso, a base para a recomendação será o Manual de Adubação e calagem para os estados do Rio Grande do Sul e Santa Catarina [@CQFSRS/SC2004].
Neste manual diz: "Os fertilizantes fosfatado e potássico indicados na adubação de pré-plantio devem ser aplicados a lanço na área total e incorporados na camada de zero a 20 cm de profundidade", pg 249.

Para teores de fósforo de Baixo, que é o caso da análise em questão, a recomendação é de 100 kg de P2O5 por ha.
Para teores de potássio alto, que é o caso desta análise, recomenda-se 25kg de potássio (K2O) por ha (tabela 1).


É necessário anotar ainda que, na edição online da embrapa [@GilbertoNava2003] ainda constam as recomendações do manual de 1994, com dosagens muito superiores ao atual (2004).

```{r echo=FALSE, include=FALSE, warning=FALSE}
library(readr)
Resultanalise2021 <- read_csv2("data/Resultanalise2021.csv", 
    skip = 4)
View(Resultanalise2021)
#head(Resultanalise2021)

library(dplyr)
limpo<-Resultanalise2021%>%
  rename( Amostra = "Amostra", 
          "Argila%" ="Argila_1",
          pH = "pH",
          P.mg="P",
           K.mg= "K mg k-1",	
          Ca.cmol="Ca",
          Mg.cmol="Mg",
          Al.cmol="Al",
          Na.cmol="Na",
          MO.porcent="MO (%)",
          H_Al.cmol="H+ Al",
          K.cmol="K cmol",
          CTC.pH7="CTC pH7",
          CTC.efet="CTC efet",
          V="V%",
          m="m%")%>%
  dplyr::select(Amostra, 
                "Argila%",
                pH,
          P.mg,
           K.mg,	
          Ca.cmol,
          Mg.cmol,
          Al.cmol,
          Na.cmol,
          MO.porcent,
          H_Al.cmol,
          K.cmol,
          CTC.pH7,
          CTC.efet,
          V,
          m)%>%
  dplyr::filter(Amostra>0)%>%
  droplevels()
  
```

Na tabela abaixo constam as primeiras linhas da tabela de dados importada.
```{r}
head(limpo)
```


## Selecionar linhaa (amostra) de interesse

Foi definida a amostra abaixo para a análise.
```{r digitar amostra, echo=FALSE, include=FALSE}
#Digite aqui no script, o número da amostra a ser analisada
AmostraAnalisar<-41

AmostraDefinida<-limpo%>%
  dplyr::filter(Amostra==AmostraAnalisar)%>%
  droplevels() 

```

```{r}
AmostraDefinida

```

# Interpretação da Análise
## pH
```{r}

if (AmostraDefinida$pH < 4.0 ){
    cat ("O pH é muito baixo")
  }else if (AmostraDefinida$pH > 4.0 & AmostraDefinida$pH < 4.4){
    cat ("O pH é baixo")
  }else if(AmostraDefinida$pH > 4.4 & AmostraDefinida$pH < 4.9){
    cat ("O pH é médio")
  }else if(AmostraDefinida$pH > 5.0 & AmostraDefinida$pH < 5.5 ){
    cat ("O pH é alto")
  }else{
    cat("O pH está muito alto")
  }
  
```

## Fósforo
Interpretação para o fósforo disponível no solo (extraído por Melich - 1) para o estado do Paraná

```{r}
library(formattable)
Interp.P <- data.frame(
  Classe.P = c("Muito baixo", 
                           "Baixo",
                           "Médio",
                           "Alto",
                           "Muito alto", "condição a evitar") ,
  "Argila<250" = c(6,12,18,24,120,120),
  "Argila250-400" = c(4,8,12,18,90,90),
  "Argila>400" = c(3,6,9,12,60,30),
"Olerícolas"=c(2,20,50,100,300,300),
"Florestais"=c(2,3,5,7,28,28),
"Pastagem.Perene.Extensiva"=c(2,3,4,10,40,40))

Interp.P
```
### CLASSE p
```{r eval=FALSE}
C_P="indefinido"

if (AmostraDefinida$`Argila%` < 250){
  if(AmostraDefinida$P.mg < Interp.P[1,2]){
      C_P=(Interp.P[1,1])
  }else (AmostraDefinida$P.mg >Interp.P[1,2] & AmostraDefinida$P.mg <Interp.P[2,2]){
      C_P=(Interp.P[2,1])
  }else (AmostraDefinida$P.mg >Interp.P[2,2] & AmostraDefinida$P.mg <Interp.P[3,2]){
      C_P=(Interp.P[3,1])
  }else (AmostraDefinida$P.mg >Interp.P[3,2] & AmostraDefinida$P.mg <Interp.P[4,2]){
      C_P=(Interp.P[4,1])
  }else (AmostraDefinida$P.mg >Interp.P[4,2] & AmostraDefinida$P.mg <Interp.P[5,2]){
      C_P= (Interp.P[5,1])
  }else (AmostraDefinida$P.mg >Interp.P[6,2]) {
      C_P= (Interp.P[6,1])
}else  (AmostraDefinida$`Argila%` > 250 & AmostraDefinida$`Argila%` <=400 ){
    if(AmostraDefinida$P.mg < Interp.P[1,3]){
      C_P=(Interp.P[1,1])
    }else (AmostraDefinida$P.mg >Interp.P[1,3] &AmostraDefinida$P.mg <Interp.P[2,3]){
      C_P= (Interp.P[2,1])
    }else (AmostraDefinida$P.mg >Interp.P[2,3] &AmostraDefinida$P.mg <Interp.P[3,3]){
      C_P= (Interp.P[3,1])
    }else (AmostraDefinida$P.mg >Interp.P[3,3] &AmostraDefinida$P.mg <Interp.P[4,3]){
      C_P= (Interp.P[4,1])
    }else (AmostraDefinida$P.mg >Interp.P[4,3] &AmostraDefinida$P.mg <Interp.P[5,3]){
      C_P= (Interp.P[5,1])
    }else (AmostraDefinida$P.mg >Interp.P[6,3]) {
      C_P= (Interp.P[6,1]) 
}else (AmostraDefinida$`Argila%` > 400 ){
   if(AmostraDefinida$P.mg < Interp.P[1,4]){
      C_P=(Interp.P[1,1])
  }else (AmostraDefinida$P.mg >Interp.P[1,4] &AmostraDefinida$P.mg <Interp.P[2,4]){
      C_P= (Interp.P[2,1])
  }else (AmostraDefinida$P.mg >Interp.P[2,4] &AmostraDefinida$P.mg <Interp.P[3,4]){
      C_P= (Interp.P[3,1])
  }else (AmostraDefinida$P.mg >Interp.P[3,4] &AmostraDefinida$P.mg <Interp.P[4,4]){
      C_P= (Interp.P[4,1])
  }else (AmostraDefinida$P.mg >Interp.P[4,4] &AmostraDefinida$P.mg <Interp.P[5,4]){
      C_P= (Interp.P[5,1])
  }else (AmostraDefinida$P.mg >Interp.P[6,4]) {
      C_P= (Interp.P[6,1])
}else{
    C_P=("Aconteceu algum erro no código")
}}}}

C_P
```


## Potássio
```{r}
Interp.K <- data.frame(
  Classe.K = c("Muito baixo", 
                           "Baixo",
                           "Médio",
                           "Alto",
                           "Muito alto",
               "condição a evitar") ,
  "K.trocavel(cmol)" = c(0.06,0.12,0.21,0.45,0.45,0),
  "%.K.trocávelCTCpH7"=c(0.5,1,2,3,10,10),
  "Olerícolas"=c(0.15,.30,.45,1.2,1.2,0))
Interp.K

```

# Recomendação para pré Plantio
## Calcário
### Pela Saturação (manual do paraná)
```{r}
AmostraDefinida<-AmostraDefinida%>%
  mutate(Calcario_PR=((.70-V)*CTC.pH7))
 

#View(AmostraDefinida)
#head(AmostraDefinida)
```


A quantidade de calcário a ser aplicado, se for PRNT 100%, é de `r (AmostraDefinida$Calcario_PR)`

A quantidade de calcário a ser aplicado, se for PRNT 75%, é de `r ((AmostraDefinida$Calcario_PR)/0.75)`

### Pelo SMP (Manual de SC e RS)
Esta análise não contém o indice SMP.

## Fósforo e Potássio
### Tabela de recomendação Paraná
```{r}

Classes.fert <- data.frame(
  Classe = c("Muito baixo", 
                           "Baixo",
                           "Médio",
                           "Alto",
                           "Muito alto") ,
  "Fósforo.kg.P2O5.ha" = c(130,100,100,75,70),
  "Potássio.kg.K2O.ha" = c(50,40,25,0,0),
  "Borax.kg.ha" = c(030,30,30,30,30))

# Classes.fert
```


```{r}
FaixaArgila<-matrix (c("<21", 4,"21 a 39,99",3, "41 a  60",2, "< 60",1), ncol=2, byrow=TRUE)
  rownames(FaixaArgila)<-c("a", "b", "c", "d")
colnames(FaixaArgila)<- c("Argila % no solo" , "Classe Argila")
FaixaArgila<-as.table(FaixaArgila)
FaixaArgila

```

```{r}
FaixaMO<-matrix (c("<2,5", "Baixo","2,6 a 5,0","Médio", "> 5", "Alto"), ncol=2, byrow=TRUE)
  rownames(FaixaMO)<-c("a", "b", "c")
colnames(FaixaMO)<- c("%MO.solo" , "Classe.MO")
FaixaMO<-as.table(FaixaMO)
FaixaMO
```



```{r}
FaixaCTC7<-matrix (c("<5", "Baixo","5 a 15,0","Médio", "> 15", "Alto"), ncol=2, byrow=TRUE)
  rownames(FaixaCTC7)<-c("a", "b", "c")
colnames(FaixaCTC7)<- c("CTC.cmolc/dm3.solo" , "Classe.CTCpH7")
FaixaMO<-as.table(FaixaCTC7)
FaixaCTC7
```


## Recomendação

# Recomendação para adubação de crescimento
Na adubação de crescimento, (até o terceiro ano, inclusive) a recomendação é a aplicação de N apenas. No primeiro ano, recomenda-se a8 kg de N, aplicados em três parcelas, sendo a primeira logo apos a brotação, a segunda 45 dias após a primeira, e a terceira 45 dias após a segunda.
No segundo ano altera-se a dosagem para 27 kg de N, e a primeira aplicação deve ser no inchamento das gemas, e as outras duas com intervalo de 45 dias.
No terceiro ano, muda-se a dose para 36 kg, com a seguência igual ao segundo ano.

# Adubação de Manutenção
Os nutrientes e as quantidades a serem aplicadas devem ser estabelecidos pela análise conjunta dos seguintes parâmetros: análise de folhas e de frutos, análise periódica de solo, idade das plantas, crescimento vegetativo, sistema de plantio e de condução, adubações anteriores, produção, exportação de nutrientes pela produção, tratos culturais, distúrbios nutricionais e presença de sintomas de deficiência ou de toxidez.

Caso seja utilizado adubo orgânico, deve-se considerar que quantidades excessivas de N e de K prejudicam a qualidade das maçãs, predispondo-as a distúrbios fisiológicos e diminuindo sua conservabilidade, além de deixar as plantas mais suscetíveis ao ataque de doenças e de pragas.

O adubo orgânico deve ser aplicado aproximadamente 30 dias antes do início da brotação.


## Uso de gesso para adicionar Calcio
"A  aplicação  de  1t/ha  de  gesso  com  15%  de  umidade  adicionaaproximadamente 200kg de cálcio, 160kg de enxofre e 8kg de fósforo naforma de P2O5. A quantidade de cálcio adicionada, neste caso, eleva o seuteor na camada de solo entre zero e 20cm em cerca de 0,5meq/100g desolo (0,5cmolc/kg). Esta informação é básica para se estimar a quantidadede gesso necessária para melhorar a relação cálcio:magnésio em soloscujo pH já foi anteriormente elevado pela calagem."[@Nuernberg2005].
Segundo apresentado no mesmo documento, o uso de 25%a 30% da dosagem de calcário, para melhorar a relação cálcio:magnésio sem elevar o pH e parasolos  cujas  camadas  inferiores  apresentam  teores  baixos  de  cálcio  eelevados de Al. 


Já no manual de SC [@CQFSRS/SC2004] indica que são necessários aplicar 3 t de gesso para elevar 1 cmolc dm-³ de 0 a 20 cm de solo, por ha.

No estudo realizado em Pato branco, por [@Danner2009] foram utilizados 80 kg de Ca+ aplicados através de diferentes fontes: cloreto de cálcio; gesso agrícola; nitrabor®;  cal hidratada e borra de celulose. Os resultados foram idênticos para todos os fertilizantes, tendo o gesso apresentado a vantagem de aumentar o teor de Ca tamém na profundidade de 15 a 30 cm.
Nos resultados é relatado o aumento de aproximadamente 1 cmolc dm-³ para os 80 kg aplicados ( de 6.5 para 7,69 cmolc dm-³).

No caso da nossa análise, temos uma concentração de Ca de 3.88 cmolc dm-³. Para elevar esta concentração para 7 (teremos uma relação de 2:1, sendo que o ideal é que seja 3:1), pecisamos 3x a dose do estudo relatado acima.

Assim, temos necessidade de aplicar 240 kg de Ca+ através de fertilizantes com este mineral. Utilizando-se o Gesso, que possui de 16 a 20% de Ca, vavos precisar de **`r 1 * 240/160` t de gesso agrícola**. 


# Referências


