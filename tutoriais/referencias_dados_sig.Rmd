---
title: "Banco de dados para SIG"
author: "Equipe"
date: "27/02/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Dados (SIG)

## IBGE

* ftp://geoftp.ibge.gov.br/
* ftp://geoftp.ibge.gov.br/cartas_e_mapas/bases_cartograficas_continuas/bc250/versao2015/Shapefile
* ftp://geoftp.ibge.gov.br/informacoes_ambientais
* https://www.ibge.gov.br/geociencias/downloads-geociencias.html
* https://mapas.ibge.gov.br/en/bases-e-referenciais/bases-cartograficas/cartas.html
* https://portaldemapas.ibge.gov.br/portal.php#mapa15666
* http://mapas.ibge.gov.br
* http://www.ppp.ibge.gov.br/ppp.htm

## Modelo digital de elevação

* https://earthexplorer.usgs.gov/
* https://www.webmapit.com.br/inpe/topodata/
* https://search.asf.alaska.edu/#/
* https://www.cnpm.embrapa.br/projetos/relevobr/download/

## Imagens de satélite

* http://earthexplorer.usgs.gov/
* https://search.remotepixel.ca/
* https://search.asf.alaska.edu/
* http://www.dgi.inpe.br/CDSR/
* http://www2.dgi.inpe.br/catalogo/explore
* https://www.usgs.gov/products/data-and-tools/real-time-data/remote-land-sensing-andlandsat
* http://www.dgi.inpe.br/CDSR/
* https://inde.gov.br/CatalogoGeoservicos

## CAR

* http://www.car.gov.br/publico/imoveis/index

## EMBRAPA

* http://mapas.cnpm.embrapa.br/somabrasil/webgis.html
* https://www.cnpm.embrapa.br/projetos/relevobr/download/

## ANA Agência Nacional das Águas

* https://metadados.ana.gov.br/geonetwork/srv/pt/main.home
* http://hidrosat.ana.gov.br/SaibaMais/Download
* http://dadosabertos.ana.gov.br/

## Ministério do Meio Ambiente Brasil MMA

* http://mapas.mma.gov.br/i3geo/datadownload.htm

## CPRM

* http://geowebapp.cprm.gov.br/ViewerWEB/

## FUNAI

* http://www.funai.gov.br/index.php/shape

## Acervo fundiário

* http://acervofundiario.incra.gov.br/acervo/acv.php
* http://acervofundiario.incra.gov.br:8080/Conversao01/

## DNIT

* http://www.dnit.gov.br/mapas-multimodais/shapefiles
* https://portalgeo.seade.gov.br/

## IBAMA

* http://siscom.ibama.gov.br/monitora_biomas/PMDBBS%20-%20CERRADO.html
* http://mapas.mma.gov.br/mapas/aplic/probio/datadownload.htm

## INPE

* http://www.dpi.inpe.br/tccerrado/dados/2013/mosaicos/

## Dados de geologia

* https://mega.nz/folder/kThXHRLJ#j18o4gKegNzs7bkPc2ezxg
* http://www.portalgeologia.com.br/index.php/mapa/#downloads-tab
* https://www2.sgc.gov.co/ProgramasDeInvestigacion/Geociencias/Paginas/GMSA.aspx
* http://www.cprm.gov.br/publique/Gestao-Territorial/Prevencao-de-DesastresNaturais/Produtos-por-Estado---Setorizacao-de-Risco-Geologico-5390.html
* http://geosgb.cprm.gov.br/

## ArcGIS Online

* https://www.arcgis.com/home/index.html

## ArcGISHUB

* http://hub.arcgis.com/

## Base SICAR

* https://www.car.gov.br/publico/municipios/downloads

## Ministério Público-RJ

* http://apps.mprj.mp.br/sistema/inloco/

## Portal Geo INEA

* https://inea.maps.arcgis.com/apps/MapSeries/index.html?appid=00cc256c620a4393b3d04d2c34acd9ed

## Clima

* https://modis.gsfc.nasa.gov/data/dataprod/mod12.php
* http://earthenginepartners.appspot.com/science-2013-global-forest
* https://www.worldclim.org/
* https://www2.ipef.br/geodatabase/

## Diversas fontes de dados

* https://sosgisbr.com/category/dados-para-download/
* http://datageo.ambiente.sp.gov.br/app/?ctx=DATAGEO#
* https://www.labgis.uerj.br/fontes_dados.php
* http://www.data.rio/
* http://www.inea.rj.gov.br/Portal/MegaDropDown/EstudosePublicacoes/EstadodoAmbiente/index.htm&lang
* http://www.cesadweb.fau.usp.br/index.php?option=com_content&view=article&id=203691&Itemid=1525
* http://www.forest-gis.com/2009/04/base-de-dados-shapefile-do-brasil-todo.html
* http://forest-gis.com/download-de-shapefiles/
* https://www.labtopope.com.br/banco-de-dados-geodesico/

## Buscar uso e ocupação Brasil

http://geo.fbds.org.br/
